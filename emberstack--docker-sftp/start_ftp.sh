#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"

USER='demo'

SFTP_CONFIG_JSON="$SH/mapped_volume/sftp.json"
cat << EOT > $SFTP_CONFIG_JSON
{
    "Global": {
        "Chroot": {
            "Directory": "%h",
            "StartPath": "sftp"
        },
        "Directories": ["sftp"]
    },
    "Users": [
        {
            "Username": "$USER",
            "Password": "$PASS"
        }
    ]
}
EOT

    source "$SH/stop_ftp.sh"
    echo
    echo "Running container $C_NAME..."
    docker run \
        --name $C_NAME \
        -p "$PORT:22" \
        \
        -v "$SFTP_CONFIG_JSON:/app/config/sftp.json:ro" \
        \
        `# upload storage` \
        -v "$SH/mapped_volume/upload_storage/$USER:/home/$USER" \
        \
        -d \
        emberstack/sftp
    echo
    docker ps | grep -E "$C_NAME|NAME" --color=always
    echo "Aftermath command
    sftp -oStrictHostKeyChecking=no  -P $PORT $USER@localhost

    #TODO need Dockerfile's config to put in ssh key file
    #NOTE after container running, adding your pub key to /home/demo/.ssh/authorized_keys will get sftp working
    "
