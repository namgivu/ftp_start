#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"

echo "Removing container $C_NAME..."
docker rm -f  $C_NAME  # 1>/dev/null 2>&1
