#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"

FTP_USER_NAME=bob
FTP_USER_PASS=122
FTP_USER_HOME="/home/ftpusers/$FTP_USER_NAME"

    source "$SH/stop_ftp.sh"
    echo
    echo "Running container $C_NAME..."
    docker run \
        --name $C_NAME \
        -p "$PORT:21" \
        -p "30000-30009:30000-30009" `# passive port` \
        \
        -e "FTP_USER_NAME=$FTP_USER_NAME" \
        -e "FTP_USER_PASS=$FTP_USER_PASS" \
        -e "FTP_USER_HOME=$FTP_USER_HOME" \
        -e 'PUBLICHOST=localhost' \
        \
        `# upload storage` \
        -v "$SH/mapped_volume/upload_storage/ftpusers:/home/ftpusers" \
        `# 1                                         :2=all ftp user be stored under this folder` \
        -d \
        stilliard/pure-ftpd
    echo
    docker ps | grep -E "$C_NAME|NAME" --color=always
    echo
    echo "Aftermath command
    ftp -p localhost 21
    "
