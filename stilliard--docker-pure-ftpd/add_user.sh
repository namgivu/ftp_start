#!/bin/bash
ref='https://github.com/stilliard/docker-pure-ftpd#example-usage-once-inside'

SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.env"

#TODO require container running $C_NAME

FTP_NEWUSER_NAME=nam
FTP_NEWUSER_PASS=122  #NOTE You will be prompted to enter password
FTP_NEWUSER_HOME="/home/ftpusers/$FTP_NEWUSER_NAME"

    echo "Adding new user to container $C_NAME..."
    echo "You will be prompted to enter password, please enter $FTP_NEWUSER_PASS"
    docker exec -it                             $C_NAME pure-pw useradd $FTP_NEWUSER_NAME -f '/etc/pure-ftpd/passwd/pureftpd.passwd'  -m -u ftpuser        -d $FTP_NEWUSER_HOME
    #           0=this allow to enter password  .       1       2       3                 4=You will be prompted to enter password    5=TODO what is this  6=storage location
