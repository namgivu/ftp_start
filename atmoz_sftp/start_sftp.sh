#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

USER1='u1'
USER2='u2'
#
source "$SH/.env"

    source "$SH/stop_sftp.sh"
    echo
    echo "Running container $C_NAME..."
    docker run \
        `# accept connection from these sshkey` \
        -v "$SH/mapped_volume/sshkey/$USER1.pub:/home/$USER1/.ssh/keys/id_rsa.pub:ro" \
        -v "$SH/mapped_volume/sshkey/$USER2.pub:/home/$USER2/.ssh/keys/id_rsa.pub:ro" \
        `# 1                                   :2                                   ` \
        \
        `# ssh host key ref. https://github.com/atmoz/sftp#providing-your-own-ssh-host-key-recommended` \
        -v "$SH/mapped_volume/sshhostkey/ssh_host_ed25519_key:/etc/ssh/ssh_host_ed25519_key" \
        -v     "$SH/mapped_volume/sshhostkey/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key"     \
        `# 1                                                 :2                            ` \
        \
        `# upload storage - CAUTION mapping at /home will NOT work aka MUST map to userhome level` \
        -v "$SH/mapped_volume/upload_storage/$USER1:/home/$USER1" \
        -v "$SH/mapped_volume/upload_storage/$USER2:/home/$USER2" \
        \
        -p "$PORT:22"   `# docker port` \
        --name $C_NAME  `# docker port` \
        -d              `# run as daemon aka background` \
        'atmoz/sftp'    `# image to run` \
        \
        `# multi-user declared here - ref to user_syntax below` \
        "$USER1::1001::upload" \
        "$USER2::1002::upload" \
        `#    ::uid`

            user_syntax='
            ref. https://github.com/atmoz/sftp#usage
            user:pass[:e][:uid[:gid[:dir1[,dir2]...]]]

            NOTE as we use keyfile to connect, param :pass can be skipped
            NOTE must provide :uid or sftp wont work
            '

    _='aftermath status check'
    sleep 2  # give sometime for the container to warmup
    echo; docker logs $C_NAME
    echo
    docker ps | grep -E "$C_NAME|NAME" --color=always

    echo
    echo "Aftermath command
    # k aka ssh_private_keyfile
    k='$SH/mapped_volume/sshkey/$USER1' ; ssh-add \$k ; sftp -oStrictHostKeyChecking=no  -P $PORT  -i \$k $USER1@localhost
    k='$SH/mapped_volume/sshkey/$USER2' ; ssh-add \$k ; sftp -oStrictHostKeyChecking=no  -P $PORT  -i \$k $USER2@localhost
    "
